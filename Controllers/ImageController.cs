
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Handwriting.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.WebUtilities;
using Handwriting.Api.Filters;
using Handwriting.Api.Utilities;
using Microsoft.Net.Http.Headers;
using System.IO;
using Handwriting.Utilities;
using System.Text;
using System.Net;
using Microsoft.AspNetCore.Http.Features;
using Google.Cloud.Vision.V1;
using Microsoft.EntityFrameworkCore;
using Google.Cloud.Storage.V1;

namespace Handwriting.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ImagesController : ControllerBase
    {
        private readonly Models.ImageContext _context;
        private readonly ILogger<ImagesController> _logger;

        private readonly long _fileSizeLimit;
        private readonly string _targetFilePath;
        private readonly string _bucketName;
        private readonly string[] _permittedExtensions = { ".png" };
        private static readonly FormOptions _defaultFormOptions = new FormOptions();

        public ImagesController(ILogger<ImagesController> logger, Models.ImageContext context, IConfiguration config)
        {
            _logger = logger;
            _context = context;

            _fileSizeLimit = config.GetValue<long>("FileSizeLimit");

            // To save physical files to a path provided by configuration:  "StoredFilesPath": "g:\\temp\\files",
            _targetFilePath = config.GetValue<string>("StoredFilesPath");

            _bucketName = config.GetValue<string>("BucketName");

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Models.Image>>> GetImage()
        {
            var images = await _context.Images.ToListAsync();

            if (images == null)
            {
                return NotFound();
            }

            return images;
        }

        // GET: api/Images/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Models.Image>> GetImage(Guid id)
        {
            var image = await _context.Images.FindAsync(id);

            if (image == null)
            {
                return NotFound();
            }

            return image;
        }

        [HttpGet("/UploadedImage/{id}")]
        public async Task<IActionResult> UploadedImage(Guid id)
        {
            var image = await _context.Images.FindAsync(id);

            if (image == null)
            {
                return NotFound();
            }

            var filePath = Path.Combine(_targetFilePath, image.StorageFileName);
            var ext = Path.GetExtension(image.StorageFileName).TrimStart();
            if (String.IsNullOrEmpty(ext)) return NotFound();

            // FileStreamResult result = null;
            var stream = new MemoryStream();
            var storage = StorageClient.Create();

            await storage.DownloadObjectAsync(_bucketName, filePath, stream);
            stream.Position = 0;
            Console.WriteLine($"filePath: {filePath}");
            return new FileStreamResult(stream, "image/" + ext);



            // return NotFound();
        }

        #region 
        [HttpPost]
        [DisableFormValueModelBinding]
        [IgnoreAntiforgeryToken]
        public async Task<ActionResult<Models.Image>> PostImage()
        {
            if (!MultipartRequestHelper.IsMultipartContentType(Request.ContentType))
            {
                ModelState.AddModelError("File",
                    $"The request couldn't be processed (Error 1).");
                // Log error
                return BadRequest(ModelState);
            }

            var boundary = MultipartRequestHelper.GetBoundary(
                MediaTypeHeaderValue.Parse(Request.ContentType),
                _defaultFormOptions.MultipartBoundaryLengthLimit);
            var reader = new MultipartReader(boundary, HttpContext.Request.Body);
            var section = await reader.ReadNextSectionAsync();
            Guid imageId = Guid.Empty;
            while (section != null)
            {
                var hasContentDispositionHeader =
                    ContentDispositionHeaderValue.TryParse(
                        section.ContentDisposition, out var contentDisposition);

                if (hasContentDispositionHeader)
                {
                    // This check assumes that there's a file
                    // present without form data. If form data
                    // is present, this method immediately fails
                    // and returns the model error.
                    if (!MultipartRequestHelper
                        .HasFileContentDisposition(contentDisposition))
                    {
                        ModelState.AddModelError("File",
                            $"The request couldn't be processed (Error 2).");
                        // Log error

                        return BadRequest(ModelState);
                    }
                    else
                    {
                        // Don't trust the file name sent by the client. To display
                        // the file name, HTML-encode the value.
                        var trustedFileNameForDisplay = WebUtility.HtmlEncode(
                                contentDisposition.FileName.Value);
                        var trustedFileNameForFileStorage = contentDisposition.FileName.Value; //Path.GetRandomFileName();
                        var filePath = Path.Combine(_targetFilePath, trustedFileNameForFileStorage);
                        // **WARNING!**
                        // In the following example, the file is saved without
                        // scanning the file's contents. In most production
                        // scenarios, an anti-virus/anti-malware scanner API
                        // is used on the file before making the file available
                        // for download or for use by other systems. 
                        // For more information, see the topic that accompanies 
                        // this sample.

                        var streamedFileContent = await FileHelpers.ProcessStreamedFile(
                            section, contentDisposition, ModelState,
                            _permittedExtensions, _fileSizeLimit);

                        if (!ModelState.IsValid)
                        {
                            return BadRequest(ModelState);
                        }


                        using (var storage = StorageClient.Create())
                        {
                            storage.UploadObject(_bucketName, filePath, null, new MemoryStream(streamedFileContent));
                            Console.WriteLine($"Uploaded {filePath}.");
                        }



                        // using (var targetStream = System.IO.File.Create(
                        //     Path.Combine(_targetFilePath, trustedFileNameForFileStorage)))
                        // {
                        //     await targetStream.WriteAsync(streamedFileContent);

                        //     _logger.LogInformation(
                        //         "Uploaded file '{TrustedFileNameForDisplay}' saved to " +
                        //         "'{TargetFilePath}' as {TrustedFileNameForFileStorage}",
                        //         trustedFileNameForDisplay, _targetFilePath,
                        //         trustedFileNameForFileStorage);
                        // }
                        var imageToBeDetected = Google.Cloud.Vision.V1.Image.FromBytes(streamedFileContent);

                        ImageAnnotatorClient client = ImageAnnotatorClient.Create();
                        IReadOnlyList<EntityAnnotation> textAnnotations = client.DetectText(imageToBeDetected);
                        foreach (EntityAnnotation text in textAnnotations)
                        {
                            Console.WriteLine($"Description: {text.Description}");
                        }

                        var image = new Models.Image(
                            Path.Combine(_targetFilePath, trustedFileNameForFileStorage),
                            trustedFileNameForDisplay,
                            trustedFileNameForFileStorage
                        );
                        image.VisionResponse = textAnnotations.FirstOrDefault().ToString();
                        image.IsProcessed = true;
                        _context.Images.Add(image);
                        await _context.SaveChangesAsync();
                        imageId = image.Id;
                    }
                }

                // Drain any remaining section body that hasn't been consumed and
                // read the headers for the next section.
                section = await reader.ReadNextSectionAsync();
            }
            Console.WriteLine($"Create At Action: {nameof(GetImage)}");

            return CreatedAtAction(nameof(GetImage), new
            {
                id = imageId
            }, imageId);
            // return Created(nameof(ImagesController), null);
        }
        #endregion

        private static void UploadFile(string bucketName, string localPath, string objectName = null)
        {
            using (var storage = StorageClient.Create())
            {

                using (var f = System.IO.File.OpenRead(localPath))
                {
                    objectName = objectName ?? Path.GetFileName(localPath);
                    storage.UploadObject(bucketName, objectName, null, f);
                    Console.WriteLine($"Uploaded {objectName}.");
                }
            }
        }
    }
}
