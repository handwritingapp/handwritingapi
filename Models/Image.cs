using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Handwriting.Api.Models
{
    public class ImageContext : DbContext
    {
        public DbSet<Image> Images { get; set; }

        // protected override void OnConfiguring(DbContextOptionsBuilder options)
        //     => options.UseSqlite("Data Source=Image.db");

        public ImageContext(DbContextOptions<ImageContext> options) : base(options)
        {
            this.Database.EnsureCreated();
        }
    }

    public class Image
    {
        public Guid Id { get; set; }
        public string Filepath { get; set; }
        public string StorageFileName { get; set; }
        public DateTime CreatedAt { get; set; }
        public string DisplayName { get; set; }
        public bool IsProcessed { get; set; }
        public string VisionResponse { get; set; }
        public string Text { get; set; }

        public Image()
        {

        }
        public Image(string filepath, string untrustedName, string storageFileName)
        {
            Id = new Guid();
            DisplayName = untrustedName;
            Filepath = filepath;
            StorageFileName = storageFileName;
            CreatedAt = DateTime.UtcNow;
        }
    }

}